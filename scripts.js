var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
//1
const nums = [1, 3, 5, 7, 9, 11, 13, 15, 17];
const biggerThanTen = nums.filter(function (num) {
    return num > 10;
});
console.log(biggerThanTen);
//2
const strings = ["JavaScript", "Typescript", "Angular", "React"];
const allUppercase = strings.map(function (str) {
    return str.toUpperCase();
});
console.log(allUppercase);
//3
const denToEur = (amount) => amount / 61.5;
console.log(denToEur(123));
//4
function data(url) {
    return __awaiter(this, void 0, void 0, function* () {
        const promise = fetch(url);
        try {
            const response = yield promise;
            const json = yield response.json();
            const elements = json.filter(function (elem) {
                return elem.completed === true;
            });
            console.log(elements.map(function (el) {
                return el.title;
            }));
        }
        catch (error) {
            console.log(error);
        }
    });
}
data('https://jsonplaceholder.typicode.com/todos/');
