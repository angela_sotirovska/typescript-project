//1
const nums: number[] = [1, 3, 5, 7, 9, 11, 13, 15, 17];

const biggerThanTen: number[] = nums.filter(function(num: number){
    return num>10;
}); 

console.log(biggerThanTen);




//2
const strings: string[] = ["JavaScript", "Typescript", "Angular", "React"];

const allUppercase: string[] = strings.map(function(str: string){
    return str.toUpperCase();
});

console.log(allUppercase);




//3
const denToEur = (amount: number) : number => amount / 61.5;
console.log(denToEur(123));




//4

async function data(url: string): Promise<void>{
    const promise: Promise<Response> = fetch(url);

    try{
        const response: Response = await promise;
        const json = await response.json();
        const elements = json.filter(function(elem){
            return elem.completed===true;
        });
        console.log(elements.map(function(el){
            return el.title;
        }));
    }
    catch (error){
        console.log(error);
    }
}

data('https://jsonplaceholder.typicode.com/todos/');
